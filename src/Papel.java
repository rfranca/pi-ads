
import javax.swing.JOptionPane;


public class Papel extends Produto implements Manipulacao {
    
    private String cor;
    private String tipo;
    private float largura;
    private float altura;
    private int gramatura;
    private boolean paltado;

    public Papel() {
        super(null, 0);
    }

    
    public Papel(String c, String t, float l, float a, int g, boolean p, String m, float v) {
        super(m, v);
        this.cor = c;
        this.tipo = t;
        this.largura = l;
        this.altura = a;
        this.gramatura = g;
        this.paltado = p;
    }

  
    /**
     * @return the cor
     */
    public String getCor() {
        return cor;
    }

    /**
     * @param cor the cor to set
     */
    public void setCor(String cor) {
        this.cor = cor;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the largura
     */
    public float getLargura() {
        return largura;
    }

    /**
     * @param largura the largura to set
     */
    public void setLargura(float largura) {
        this.largura = largura;
    }

    /**
     * @return the altura
     */
    public float getAltura() {
        return altura;
    }

    /**
     * @param altura the altura to set
     */
    public void setAltura(float altura) {
        this.altura = altura;
    }

    /**
     * @return the gramatura
     */
    public int getGramatura() {
        return gramatura;
    }

    /**
     * @param gramatura the gramatura to set
     */
    public void setGramatura(int gramatura) {
        this.gramatura = gramatura;
    }
    
    /**
     * @param paltado the paltado to set
     */
    public void setPaltado(boolean paltado) {
        this.paltado = paltado;
    }

    /**
     * @return the paltado
     */
    public boolean isPaltado() {
        return paltado;
    }
   
    @Override
    public String consulta() {
        String msg;
        msg = "Marca: " + this.getMarca();
        msg += "\nValor: $" + this.getValor();
        msg += "\nCor: " + this.cor;
        msg += "\nTipo: " + this.tipo;
        msg += "\nLargura: " + this.largura;
        msg += "\nAltura: " + this.altura;
        msg += "\nGramatura: " + this.gramatura;
        msg += "\nPaltado: " + this.paltado;

        return msg;
    }

    @Override
    public boolean cadastro() {
        this.setMarca(JOptionPane.showInputDialog("Digite a marca:"));
        this.setValor(Float.parseFloat(JOptionPane.showInputDialog("Digite o valor:")));
        this.setCor(JOptionPane.showInputDialog("Digite a cor:"));
        this.setTipo(JOptionPane.showInputDialog("Digite a Tipo"));
        this.setLargura(Float.parseFloat(JOptionPane.showInputDialog("Digite a largura")));
        this.setAltura(Float.parseFloat(JOptionPane.showInputDialog("Digite a altura")));
        this.setGramatura(Integer.parseInt(JOptionPane.showInputDialog("Digite a gramatura")));
        this.setPaltado(Boolean.parseBoolean(JOptionPane.showInputDialog("O caderno é paltado?\n 1 - com palta \n0 -  sem palta ")));
        return true;
    }
    
    @Override
    public String getMarca() {
        return super.getMarca();
    }

    @Override
    public void setMarca(String marca) {
        super.setMarca(marca);
    }

    @Override
    public float getValor() {
        return super.getValor();
    }

    @Override
    public void setValor(float valor) {
        super.setValor(valor);
    }
    
    
    
    
}
