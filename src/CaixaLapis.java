import javax.swing.JOptionPane;

public class CaixaLapis extends Produto implements Manipulacao {
    
    private int quantidade;
    private boolean colorido;

    public CaixaLapis() {
        super(null, 0);
    }

    public CaixaLapis(int quantidade, boolean colorido, String m, float v) {
        super(m, v);
        this.quantidade = quantidade;
        this.colorido = colorido;
    }

    /**
     * @return the quantidade
     */
    public int getQuantidade() {
        return quantidade;
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    /**
     * @param colorido the colorido to set
     */
    public void setColorido(boolean colorido) {
        this.colorido = colorido;
    }

    /**
     * @return the colorido
     */
    public boolean isColorido() {
        return colorido;
    }
           
    @Override
    public String consulta() {
        String msg;
        msg = "Marca: " + this.getMarca();
        msg += "\nValor: $" + this.getValor();
        msg += "\nQuantidade: " + this.quantidade;
        msg += "\nColorido: " + this.colorido;

        return msg;
    }

    @Override
    public boolean cadastro() {
        this.setMarca(JOptionPane.showInputDialog("Digite a marca:"));
        this.setValor(Float.parseFloat(JOptionPane.showInputDialog("Digite o valor:")));
        this.setQuantidade(Integer.parseInt(JOptionPane.showInputDialog("Digite a quantidae:")));
        this.setColorido(Boolean.parseBoolean(JOptionPane.showInputDialog("A caixa de lápis é colorido a\n 1 - sim \n0 - não")));
        return true;
    }
    
    @Override
    public String getMarca() {
        return super.getMarca(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMarca(String marca) {
        super.setMarca(marca); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float getValor() {
        return super.getValor(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setValor(float valor) {
        super.setValor(valor); //To change body of generated methods, choose Tools | Templates.
    }

}
