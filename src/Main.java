
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author rodrigofranca
 */
public class Main {

    
    
    public static void main(String[] args) {
        
        ArrayList<Pedido> ped = new ArrayList ();
        
        
        int opt, nPedido;
        
        do {
            String msg;
            msg = "Selecione o tipo de produto:"
             + "\n1 - Cadastrar Pedido"
             + "\n2 - Listar Pedidos"
             + "\n3 - Sair";
            
            opt = Integer.parseInt(JOptionPane.showInputDialog(msg));
            
            switch (opt) {
                case 1:
                    Pedido p = new Pedido();
                    p.cadastro();
                    ped.add(p);
                    break;
                case 2:
                    
                    msg = "";
                    int t = 1;
                    for (int z=0; z < ped.size(); z++) {
                        msg += "Pedido Número: ";
                        msg += t + " de   " + ped.get(z).getCliente().getNome() +"\n\n";
                        t++;
                    }
                    msg += "Informe o número do pedido para mostrar a lista de itens\n\n";
                    nPedido = Integer.parseInt(JOptionPane.showInputDialog(msg));
                    --nPedido;
                        System.out.println(ped.get(nPedido).consulta());
                    break;
            }
        }while(opt!=3);
        
        
        
    }
    
}
