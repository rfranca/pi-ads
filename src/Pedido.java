import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Pedido implements Manipulacao{

    private Data data;
    private Cliente cliente;
    private float totalpedido;
    
    private ArrayList <Caderno> cadernoList = new ArrayList();
    private ArrayList <CaixaLapis> caixaLapisList = new ArrayList();
    private ArrayList <Papel> papelList = new ArrayList();

    public Pedido() {
    }

    public Pedido(Data data, Cliente cliente, float totalpedido) {
        this.data = data;
        this.cliente = cliente;
        this.totalpedido = totalpedido;
    }
    
    @Override
    public boolean cadastro() {
        
        Data d = new Data();
        d.setDia(Integer.parseInt(JOptionPane.showInputDialog("Dia:")));
        d.setMes(Integer.parseInt(JOptionPane.showInputDialog("Mês:")));
        d.setAno(Integer.parseInt(JOptionPane.showInputDialog("Ano:")));
        data = d;
        
        Cliente c = new Cliente();
        
        c.setNome(JOptionPane.showInputDialog("Cliente nome: "));
        c.setCpf(JOptionPane.showInputDialog("Cliente CPF: "));
        c.setTelefone(JOptionPane.showInputDialog("Cliente Telefone: "));
        cliente = c;
        
        int op;
        String marca, tamanho, tipo, cor;
        float valor, largura, altura;
        int qtde, gramatura, status;
        boolean statusTratado;
                
        do {
            String msg;
            msg = "Selecione o tipo de produto:"
             + "\n1 - Caderno"
             + "\n2 - Caixa de Lápis"
             + "\n3 - Papel"
             + "\n4 - Voltar ao Menu Principal";
            op = Integer.parseInt(JOptionPane.showInputDialog(msg));
            
            switch (op) {
                case 1:
                    //CADERNO
                    marca = JOptionPane.showInputDialog("Marca?");
                    valor = Float.parseFloat(JOptionPane.showInputDialog("Valor?"));
                    qtde = Integer.parseInt(JOptionPane.showInputDialog("Quantidade de Folhas? "));
                    tamanho = JOptionPane.showInputDialog("Tamanho?");
                    tipo = JOptionPane.showInputDialog("Tipo?");
                    status = Integer.parseInt(JOptionPane.showInputDialog("Tem capa dura:\n1 - Sim\n0 - Não"));
                    statusTratado = false;
                    if (status == 1) {
                        statusTratado = true;
                    }
                    Caderno cad = new Caderno(qtde, tamanho, tipo, statusTratado, marca, valor);
                    cadernoList.add(cad);
                    break;
                case 2:
                    //CAIXA DE LAPIS
                    marca = JOptionPane.showInputDialog("Marca?");
                    valor = Float.parseFloat(JOptionPane.showInputDialog("Valor?"));
                    qtde = Integer.parseInt(JOptionPane.showInputDialog("Quantidade de lápis? "));
                    status = Integer.parseInt(JOptionPane.showInputDialog("É colorido :\n1 - Sim\n0 - Não"));
                    statusTratado = false;
                    if (status == 1) {
                        statusTratado = true;
                    }
                    CaixaLapis cl = new CaixaLapis(qtde, statusTratado, marca, valor);
                    caixaLapisList.add(cl);
                    break;
                case 3:
                    //PAPEL
                    marca = JOptionPane.showInputDialog("Marca?");
                    valor = Float.parseFloat(JOptionPane.showInputDialog("Valor?"));
                    cor = JOptionPane.showInputDialog("Cor?");
                    tipo = JOptionPane.showInputDialog("Tipo?");
                    largura = Float.parseFloat(JOptionPane.showInputDialog("Largura?"));
                    altura = Float.parseFloat(JOptionPane.showInputDialog("Altura?"));
                    gramatura = Integer.parseInt(JOptionPane.showInputDialog("Gramatura?"));
                    status = Integer.parseInt(JOptionPane.showInputDialog("É paltado:\n1 - Sim\n0 - Não"));
                    statusTratado = false;
                    if (status == 1) {
                        statusTratado = true;
                    }
                    Papel p1 = new Papel(cor, tipo, largura, altura, gramatura, statusTratado, marca, valor);
                    papelList.add(p1);
                    break;
            }
        } while (op!=4);
        
        
        return true;
    }

    @Override
    public String consulta() {
        //CLIENTE
        String msg;
        int item = 1;
        msg = "\n========= PEDIDO =========\n";
        msg += "\nDATA: " + this.data.getDia() + "/" + this.data.getMes() + "/"+ this.data.getAno() + "\n\n" ;
        
        msg += "\n========= DADOS CLIENTE =========\n";
        msg += "\nNome: " + this.cliente.getNome();
        msg += "\nCPF: " + this.cliente.getCpf();
        msg += "\nTelefone: " + this.cliente.getTelefone();
        
        
        if (this.getCadernoList().size() > 0) {
            msg += "\n========= CONTEÚDO PEDIDO CADERNO =========\n";
            for(int i = 0; i < this.getCadernoList().size(); i++ ){
                msg += "\n\nITEM NUMERO: " + item + "\n";
                msg += this.getCadernoList().get(i).consulta();
                item++;
            }
        }
       
        if (this.getCaixaLapisList().size() > 0) {
            msg += "\n========= CONTEÚDO PEDIDO CAIXA LÁPIS =========\n";
            
            for(int i = 0; i < this.getCaixaLapisList().size(); i++ ){
                msg += "\n\nITEM NUMERO: " + item + "\n";
                msg += this.getCaixaLapisList().get(i).consulta();
                item++;
            }
        }
        
        if (this.getPapelList().size() > 0) {
            msg += "\n========= CONTEÚDO PEDIDO PAPEL =========\n";
            
            for(int i = 0; i < this.getPapelList().size(); i++ ){
                msg += "\n\nITEM NUMERO: " + item + "\n";
                msg += this.getPapelList().get(i).consulta();
                item++;
            }
        }
        this.calculaTotalPedido();
        if ( this.getTotalpedido() > 0 ) {
            
            msg += "\n========= TOTAL =========\n";
            msg += "R$ " + this.getTotalpedido();
        }
        return msg;
    }
    
    
    public void calculaTotalPedido()
    {
        float totalPedido;
        float imp;
        
        totalPedido = 0;
        for(int i = 0; i < this.getCadernoList().size(); i++ ){
            totalPedido += this.getCadernoList().get(i).getValor();
        }
        
        for(int i = 0; i < this.getCaixaLapisList().size(); i++ ){
            totalPedido += this.getCaixaLapisList().get(i).getValor();
        }
        
        for(int i = 0; i < this.getPapelList().size(); i++ ){
            totalPedido += this.getPapelList().get(i).getValor();
        }
        
        imp = totalPedido * Float.parseFloat("0.18");
        this.totalpedido = totalPedido + imp; 
    }

    /**
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }

    /**
     * @return the data
     */
    public Data getData() {
        return data;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }
    
    /**
     * @return the totalpedido
     */
    public float getTotalpedido() {
        return totalpedido;
    }

    /**
     * @param totalpedido the totalpedido to set
     */
    public void setTotalpedido(float totalpedido) {
        this.totalpedido = totalpedido;
    }

    public void setCadernoList(ArrayList<Caderno> cadernoList) {
        this.cadernoList = cadernoList;
    }

    public ArrayList<Caderno> getCadernoList() {
        return cadernoList;
    }

    public ArrayList<CaixaLapis> getCaixaLapisList() {
        return caixaLapisList;
    }

    public void setCaixaLapisList(ArrayList<CaixaLapis> caixaLapisList) {
        this.caixaLapisList = caixaLapisList;
    }

    public ArrayList<Papel> getPapelList() {
        return papelList;
    }

    public void setPapelList(ArrayList<Papel> papelList) {
        this.papelList = papelList;
    }
}
