import javax.swing.JOptionPane;

//FALTA METODOS DE ACESSO
public class Caderno extends Produto implements Manipulacao {
    
    private int qtdefolhas;
    private String tamanho;
    private String tipo;
    private boolean capadura;

    public Caderno() {
        super(null, 0);
    }

    public Caderno(int qtdefolhas, String tamanho, String tipo, boolean capadura, String m, float v) {
        super(m, v);
        this.qtdefolhas = qtdefolhas;
        this.tamanho = tamanho;
        this.tipo = tipo;
        this.capadura = capadura;
        
    }
    
    /**
     * @param qtdefolhas the qtdefolhas to set
     */
    public void setQtdefolhas(int qtdefolhas) {
        this.qtdefolhas = qtdefolhas;
    }

    /**
     * @return the qtdefolhas
     */
    public int getQtdefolhas() {
        return qtdefolhas;
    }

    /**
     * @param tamanho the tamanho to set
     */
    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    /**
     * @return the tamanho
     */
    public String getTamanho() {
        return tamanho;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param capadura the capadura to set
     */
    public void setCapadura(boolean capadura) {
        this.capadura = capadura;
    }
    
    /**
     * @return the capadura
     */
    public boolean isCapadura() {
        return capadura;
    }
    
    
   @Override
    public String consulta() {
        String msg;
        msg = "Marca: " + this.getMarca();
        msg += "\nValor: $" + this.getValor();
        msg += "\nQuantidade de Folhas: " + this.qtdefolhas;
        msg += "\nTamanho: " + this.tamanho;
        msg += "\nTipo: " + this.tipo;
        msg += "\nCapadura: " + this.capadura;

        return msg;
    }

    @Override
    public boolean cadastro() {
        this.setMarca(JOptionPane.showInputDialog("Digite a marca:"));
        this.setValor(Float.parseFloat(JOptionPane.showInputDialog("Digite o valor:")));
        this.setQtdefolhas(Integer.parseInt(JOptionPane.showInputDialog("Digite a quantidade de folhas:")));
        this.setTamanho(JOptionPane.showInputDialog("Digite o tamanho:"));
        this.setTipo(JOptionPane.showInputDialog("Digite o tipo:"));
        this.setCapadura(Boolean.parseBoolean(JOptionPane.showInputDialog("O caderno tem capa dura a\n 1 - sim \n0 - não")));
        return true;
    }
    
    @Override
    public String getMarca() {
        return super.getMarca(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMarca(String marca) {
        super.setMarca(marca); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float getValor() {
        return super.getValor(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setValor(float valor) {
        super.setValor(valor); //To change body of generated methods, choose Tools | Templates.
    }
}
